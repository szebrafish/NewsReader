package com.zebra.newsreader;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.IOUtils;

public class MainActivity extends AppCompatActivity {

    ListView listViewTitles;

    static SQLiteDatabase dbArticles;

    Cursor cursorMain;

    SimpleCursorAdapter dbAdapter;

    //Should be max # of articles downloaded and saved
    int maxListItems = 40;


    public class DownloadTaskArticles extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String source = "";
            String articleID = "";
            String articleTitle = "";
            String articleURL = "";
            String articleHTML = "";

            ContentValues contentValues = new ContentValues();

            //Full download task for JSON and HTML
            //Will download and parse JSON, download article HTMLs, and save to SQL database
            try {
                //Downloads 'Top Stories' JSON - actually changed to 'New Stories'
                URL url = new URL("https://hacker-news.firebaseio.com/v0/newstories.json");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();

                source = IOUtils.toString(inputStream, "UTF-8");

                //Parses ^^ JSON into array for looping through articles
                JSONArray jsonTopStories = new JSONArray(source);

                //Sets maxDownloads as JSON size in case JSON retrieved is less than maxListItems
                int maxDownloads = maxListItems;
                if (jsonTopStories.length() < maxDownloads) {

                    maxDownloads = jsonTopStories.length();

                }

                //Loops through JSON to downloads article title/URL and then full HTML
                for (int jsonTopStoriesCounter = 0; jsonTopStoriesCounter < maxDownloads; jsonTopStoriesCounter++) {

                    articleID = jsonTopStories.getString(jsonTopStoriesCounter);

                    url = new URL("https://hacker-news.firebaseio.com/v0/item/" + articleID + ".json");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    inputStream = urlConnection.getInputStream();

                    source = IOUtils.toString(inputStream, "UTF-8");

                    JSONObject jsonStory = new JSONObject(source);

                    //Runs final code if required tags are found (sometimes the JSON returned did not include "url" field)
                    //Lack of either "title" or "url" should skip seamlessly to next articleID
                    if (!jsonStory.isNull("title") && !jsonStory.isNull("url")){

                        articleTitle = jsonStory.getString("title");
                        articleURL = jsonStory.getString("url");

                        //Checks if article title already exists in database
                        //If article is new, downloads HTML and saves to SQL
                        Cursor cursorCheckDuplicate = dbArticles.rawQuery("SELECT _id FROM articles WHERE title = ?", new String[]{articleTitle});
                        if (!((cursorCheckDuplicate != null) && (cursorCheckDuplicate.getCount() > 0))) {

                            url = new URL(articleURL);
                            urlConnection = (HttpURLConnection) url.openConnection();
                            inputStream = urlConnection.getInputStream();

                            articleHTML = IOUtils.toString(inputStream, "UTF-8");

                            //Saves Title & HTML to SQL
                            contentValues.clear();
                            contentValues.put("title", articleTitle);
                            contentValues.put("html", articleHTML);
                            dbArticles.insert("articles", null, contentValues);

                            //Used for testing
                            //Log.i("Title Downloaded", articleTitle);

                            //Trim DB length to maxListItems size, keeping newest by timestamp
                            dbArticles.delete("articles", "time NOT IN (SELECT time FROM articles ORDER BY time DESC LIMIT ?)", new String[]{Integer.toString(maxListItems)});

                            //Reloads cursor query to prepare for below ListView update
                            cursorMain = dbArticles.rawQuery("SELECT * FROM articles ORDER BY time DESC, _id DESC", null);

                            //Reloads ListView (on the UI thread)
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    dbAdapter.swapCursor(cursorMain);
                                }
                            });
                        }
                        cursorCheckDuplicate.close();
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(MainActivity.this, "Internet required to update articles.\r\nExisting articles are still available offline.", Toast.LENGTH_LONG).show();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewTitles = (ListView) findViewById(R.id.listViewTitles);

        dbArticles = this.openOrCreateDatabase("Articles.db", MODE_PRIVATE, null);

        dbArticles.execSQL("CREATE TABLE IF NOT EXISTS articles (title VARCHAR, html VARCHAR, time DATETIME DEFAULT CURRENT_TIMESTAMP, _id INTEGER PRIMARY KEY)");

        //Use for testing to wipe DB
        //dbArticles.execSQL("DELETE FROM articles WHERE _id >= 0");

        DownloadTaskArticles downloadArticles = new DownloadTaskArticles();
        downloadArticles.execute();

        cursorMain = dbArticles.rawQuery("SELECT * FROM articles ORDER BY time DESC, _id DESC", null);

        dbAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursorMain, new String[]{"title"}, new int[]{android.R.id.text1}, 0);

        listViewTitles.setAdapter(dbAdapter);

        listViewTitles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), ReaderActivity.class);
                intent.putExtra("Position", position);
                startActivity(intent);

            }
        });
    }
}
