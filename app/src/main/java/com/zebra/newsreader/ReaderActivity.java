package com.zebra.newsreader;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ReaderActivity extends AppCompatActivity {

    WebView webViewArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        webViewArticle = (WebView) findViewById(R.id.webViewArticle);
        webViewArticle.getSettings().getJavaScriptEnabled();
        webViewArticle.setWebViewClient(new WebViewClient());

        Intent intent = getIntent();
        int position = intent.getIntExtra("Position", 0);

        //Recreates cursor and moves to row passed by intent ^^
        Cursor cursor = MainActivity.dbArticles.rawQuery("SELECT * FROM articles ORDER BY time DESC, _id DESC", null);
        cursor.moveToPosition(position);

        //Loads HTML stored in SQL to webview
        webViewArticle.loadData(cursor.getString(cursor.getColumnIndex("html")), "text/html", "UTF-8");
    }
}
